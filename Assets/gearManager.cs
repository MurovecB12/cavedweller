﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gearManager : MonoBehaviour {

	float gearScore=0;
	int velikostPolinoma = 5;
	int dolzinaDMGFunkcije = 31; // koliko točk lahko damo največ v orožje "maks indeks = 30"
	static int attributePointsRemaining = 30;
	string izpis;
	public GameObject histogramImageObject; 

	float[] koeficientiPolinoma; //pozicija v seznamu je potenca clena
	float[] DMGFunction;

	void Awake(){
		DMGFunction = new float[dolzinaDMGFunkcije];
		ustvariDMGFunction (-10,13);
		pokvariVrednostDMGFunkcije ();
	}


/// <summary>
/// Pristeje stevilo n atributu gearScore v gearSlot-u.
/// </summary>
/// <param name="n">velikost spremembe GS.</param>
	public void nastaviGearScore(int n){
		if (attributePointsRemaining - n >= 0 && gearScore + n >= 0) {
			gearScore += n;
			attributePointsRemaining -=n;
			Text[] arrayTextov = this.GetComponentsInChildren<Text>();
			foreach(Text t in arrayTextov){
				//GetComponent poisce vse komponente ki so Text, potem z foreach gremo cez seznam
				//in t ima komponento name "ime" ki ga primerjamo da dobimo tapravo, pristejemo
				if(t.name.Equals ("gearScore")){
					t.text = gearScore.ToString();
				}
			}
		}
	}

	public void plusGumbFunkcija(){
		nastaviGearScore (1);
		statsMeniButton ();
	}
	public void minusGumbFunkcija(){
		nastaviGearScore (-1);
		statsMeniButton ();
	}
	public void bigPlusGumbFunkcija(){
		nastaviGearScore (5);
		statsMeniButton ();
	}
	public void bigMinusGumbFunkcija(){
		nastaviGearScore (-5);
		statsMeniButton ();
	}

	public float izracunajVrednostPolinoma(){
		float vrednost = 0;
		for (int i=0; i<koeficientiPolinoma.Length; i++) {
			vrednost += koeficientiPolinoma[i]*(Mathf.Pow (gearScore,i));
		}
		return vrednost;
	}

	public void ustvariDMGFunction(int min, int max){
		float tekocaUsota = 0;
		float k = 0;
		for (int i = 0; i < dolzinaDMGFunkcije; i++) {
			k = Random.Range (min, max);
			DMGFunction [i] = tekocaUsota + k;
			tekocaUsota += k;
		}
	}

	public void pokvariVrednostDMGFunkcije(){
		izpis = "";
			for(int i = 0; i < dolzinaDMGFunkcije; i++){
				if(Random.Range(0,1)<0.1){
					DMGFunction[i] -= DMGFunction[i]/3;
				}
				if(Random.Range(0,1)<0.25){
					DMGFunction[i] += DMGFunction[i]/2;
				}
			}
		foreach(float stevka in DMGFunction){
			izpis += stevka.ToString() + " ";
		}

	}

	public float vrniVrednostDMGFunkcije(){
		return DMGFunction [(int)gearScore];
	}

	public void statsMeniButton(){
		// spremini ime "Ime itema" v izbran item
		GameObject.Find ("statsItemName").GetComponent<Text>().text = name;

		GameObject.Find ("MaxDPS").GetComponent<Text>().text=Mathf.Max(DMGFunction).ToString();
		GameObject.Find ("MinDPS").GetComponent<Text>().text=Mathf.Min(DMGFunction).ToString();
		nastaviStatPointText ();

		//zbrise prejsnje histograme
		foreach(GameObject hist in GameObject.FindGameObjectsWithTag ("histogramTag")){
			Destroy(hist);
		}

		// narise histogram" 
		for(int i=0;i<dolzinaDMGFunkcije-1;i++){
			GameObject temp = Instantiate(histogramImageObject);
			temp.transform.SetParent(GameObject.Find ("statsDisplayImage").transform);
	
			float razmerje = DMGFunction[i]/Mathf.Max(Mathf.Max(DMGFunction),Mathf.Abs(Mathf.Min (DMGFunction)));//maksimum abs vrednosti v areju
			temp.transform.localScale=new Vector3(1,razmerje,0);

			if(gearScore==i){
				temp.GetComponent<Image>().color = Color.blue;
			}else{
				temp.GetComponent<Image>().color = Color.green;
			}
		}
	}


	public void nastaviStatPointText(){
		GameObject.Find ("statsPointsRemaining").GetComponent<Text>().text="Attribute points: " + attributePointsRemaining;
	}




}




