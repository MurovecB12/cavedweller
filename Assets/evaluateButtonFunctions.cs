﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class evaluateButtonFunctions : MonoBehaviour {
	public static float DMG=0;

	void Start(){
		evaluateGear ();
	}
	public void evaluateGear(){
		DMG = 0;
		GameObject[] gear = GameObject.FindGameObjectsWithTag("gearItem");

		foreach (GameObject item in gear) {
			DMG += item.GetComponent<gearManager>().vrniVrednostDMGFunkcije();
		}

		GameObject.Find ("DPS").GetComponent <Text>().text = "DPS: "+ DMG.ToString();
		GameObject.Find ("hero").GetComponent<cracacter> ().setDMG (evaluateButtonFunctions.DMG);
	}



}
