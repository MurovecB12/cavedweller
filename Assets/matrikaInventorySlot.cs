﻿using UnityEngine;
using System.Collections;

public class matrikaInventorySlot : MonoBehaviour {

	float[] DPSFunkcija = new float[30];

	public void setDPSFunkcija(float[] DPSFunkcija){
		this.DPSFunkcija = DPSFunkcija;
	}

	public float[] getDPSFunkcija(){
		return DPSFunkcija;
	}

	public matrikaInventorySlot(int mini, int maksi){
		this.DPSFunkcija = randomFunction (mini,maksi);
	}

	public float[] randomFunction(int mini, int maksi){
		float tekocaUsota = 0;
		float k = 0;
		float[] vrniDPS= new float[30];
		for (int i = 0; i < 30; i++) {
			k = Random.Range (mini, maksi);
			vrniDPS[i] = tekocaUsota + k;
			tekocaUsota += k;
		}
		pokvariVrednostDMGFunkcije (vrniDPS);
		return vrniDPS;
	}

	public void pokvariVrednostDMGFunkcije(float[] pokvari){
		for(int i = 0; i < 30; i++){
			if(Random.Range(0,1)<0.1){
				pokvari[i] -= pokvari[i]/3;
			}
			if(Random.Range(0,1)<0.25){
				pokvari[i] += pokvari[i]/2;
			}
		}
	}

}
