﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

	public matrikaInventorySlot[] DPSFunctionInventory=new matrikaInventorySlot[20];
	public int VelikostTrenutInv = 0;
	public GameObject inventoryWindow;
	public GameObject inventorySlot;
	GameObject GearSlot;

	
	public void openInventory(){
		inventoryWindow.SetActive (true);
		fillInventory ();
	}

	public void closeInventory(){
		foreach (Transform t in GameObject.Find ("Inventory").GetComponentsInChildren<Transform>()) {
			if(t.name.Equals("Inventory")){}
			else {
				Destroy(t.gameObject);
			}
		}
		GameObject.Find("InventoryPanel").SetActive (false);

	}

	public void fillInventory(){
		if (VelikostTrenutInv == 0) {
			GameObject temp;
			temp = Instantiate (inventorySlot) as GameObject;
			temp.GetComponentInChildren<Text> ().text = "Get back to work!";
			temp.transform.SetParent (GameObject.Find ("Inventory").transform);
		} else {
			for (int i=0;i< VelikostTrenutInv;i++) {
				float[] DPSFunction = DPSFunctionInventory[i].getDPSFunkcija();
				GameObject temp;
				string imeItema = generateName ();
				temp = Instantiate (inventorySlot) as GameObject;
				temp.GetComponentInChildren<Text> ().text = "<" + (int)average (DPSFunction) +  "> " + imeItema + 
					" [" + Mathf.Min (DPSFunction) + ", " + Mathf.Max (DPSFunction) + "]";
				temp.transform.SetParent (GameObject.Find ("Inventory").transform);

			}
		}

	}

	public string generateName(){
		int i = Random.Range (0, pridevniki.Length);
		int j = Random.Range (0, samostalniki.Length);
		int k = Random.Range (0, ofSomething.Length);
		return pridevniki [i] + " " + samostalniki [j] + " of " + ofSomething [k];

	}

	public float average(float[] function){
		float vsota = 0;
		foreach (float i in function) {
			vsota+=i;
		}
		return vsota / function.Length;
	}


	//REZERVOAR IMEN//

	string[] pridevniki={"Big","Small","Icy","Fiery","Sharp","Bland","Dragonite","Blocky","Slient","Round",
		"Jagged","Dull","Exceptional","Woodland","Fallen","Elemental","Windy","Spectral","Normal","Deathly","Sucky","Crappy"};
	string[] samostalniki={"Crystal","Shard","Rock","Sludge","Slime","Quasicrystal","Dust","Gravel","Coating","Poison","Enchantment"};
	string[] ofSomething ={"Doom","Exellence","Mediocracy","Idiocracy","Ice","Death","Fire","the Arcane","Cashew",
		"the Warrior","the Mage","the Thief","the Dungeon Lord","Lord Mougli","Count Duckula","Dackuly","Gargamel"};







}
