﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class fightManager : MonoBehaviour {

	cracacter hero;
	cracacter enemy;
	bool fighting = false;
	bool win = false;
	int stopnja=1;
	int tezavnost=1;
	string stopTezavnostText;
	public InventoryManager inventory;

	void Awake(){
		hero = GameObject.Find("hero").GetComponent<cracacter>();
		enemy = GameObject.Find("enemy").GetComponent<cracacter>();
	}

	public void fightButton(){
		hero.health = 4000;
		if (!fighting) {
			StartCoroutine(fight ());
		}
	}

	IEnumerator fight(){

		win = false;
		fighting = true;
		while (true) {

			hero.dealDamage(enemy);
			if(enemy.health < 0){
				break;
			}
			yield return new WaitForSeconds (0.1f);
			enemy.dealDamage(hero);
			if(hero.health < 0){
				break;
			}
			yield return new WaitForSeconds (0.1f);

		}
			fighting = false;
		if (hero.health >= 0) {
			win = true;
			Debug.Log(inventory.VelikostTrenutInv);
			inventory.DPSFunctionInventory[inventory.VelikostTrenutInv]=new matrikaInventorySlot(-10,stopnja+tezavnost+1);
			inventory.VelikostTrenutInv+=1;
		}
	}

	public void plusStopnjaGumb(){
		stopnja += 1;
		enemy.generateEnemy (stopnja, tezavnost);
		enemy.posodobiStateCracacter ();
		posodobiStopTezavnostText ();
	}
	public void minusStopnjaGumb(){
		if(stopnja >0){
			stopnja -= 1;
			enemy.generateEnemy (stopnja, tezavnost);
			enemy.posodobiStateCracacter ();
			posodobiStopTezavnostText ();
			}else{}
	}
	public void plusTezavnostGumb(){
		tezavnost += 1;
		enemy.generateEnemy (stopnja, tezavnost);
		enemy.posodobiStateCracacter ();
		posodobiStopTezavnostText ();
	}
	public void minusTezavnostGumb(){
		if (tezavnost > 0) {
			tezavnost -= 1;
			enemy.generateEnemy (stopnja, tezavnost);
			enemy.posodobiStateCracacter ();
			posodobiStopTezavnostText ();
		} else {}
	}

	public void posodobiStopTezavnostText(){
		GameObject.Find ("enemyButton").GetComponentInChildren<Text> ().text = "tezavnost:" + tezavnost +"\n"+  "stopnja:"+stopnja;
	}

	public void nextEnemyGumb(){
		enemy.generateEnemy (stopnja, tezavnost);
		enemy.posodobiStateCracacter ();
	}




}
