﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class cracacter : MonoBehaviour {
	
	public float dmg = 150;
	public float health=4000;


	void Awake(){
		if (name.Equals ("hero")) {
			dmg = evaluateButtonFunctions.DMG;
			health=4000;
		} else {
			generateEnemy(1,1);
				}
		posodobiStateCracacter ();
	}

	public void takeDamage(float dmg){
		health -= dmg;
		foreach (Text comp in GetComponentsInChildren<Text>()) {
			if (comp.name.Equals ("TextHealth")) {
				comp.text = health.ToString ();
			}
		}
	}

	public void dealDamage(cracacter enemy){
		enemy.takeDamage (dmg);
	}

	public void setDMG(float setter){
		dmg = setter;
		foreach (Text comp in GetComponentsInChildren<Text>()) {
			if(comp.name.Equals("TextHealth")){
				comp.text = health.ToString();
			}else{
				comp.text = dmg.ToString();
			}
		}
	}


	public float normalDistribution(float mean, float se){
		//Box Muller transform
		float u1 = Random.Range(1,1001)/1000f; //porazdelitev uniform(0,1) random 
		float u2 = Random.Range(1,1001)/1000f;
		float randNormal = Mathf.Sqrt(-2 * Mathf.Log(u1)) *Mathf.Sin(2 * Mathf.PI * u2); //porazdelitev normal(0,1)
		return mean + se * randNormal; //porazdeljena normal(mean,se**2)
	}

	public void generateEnemy(int stopnja,int tezavnost){
		//nastavi health in dmg enemija
		health = (int)normalDistribution(500*(stopnja+tezavnost),500);
		dmg = (int)normalDistribution(10*(stopnja+tezavnost),10);

	}

	public void posodobiStateCracacter(){
		foreach (Text comp in GetComponentsInChildren<Text>()) {
			if(comp.name.Equals("TextHealth")){
				comp.text = health.ToString();
			}else{
				comp.text = dmg.ToString();
			}
		}
	}





}
