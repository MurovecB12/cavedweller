# README #

# Sodelujoči #
* Benjamin Medvešček Murovec
* Dik Medvešček Murovec

# Povzetek #
* Ideja: Vsakič ko premagamo nasprotnika dobimo kos opreme, ki ima randomizirane vrednosti.
* Borimo se proti nasprotnikom, nabiramo opremo, iščemo najboljše vrednosti opreme za čim manj točk.

# Opis #
* Na levi strani imamo našega bojevnika(oz. njegovo opremo), na desni je polje, kjer se bojujemo.
* S pritiskom na gumb "Stats" prikažemo vrednost naše trenutne opreme. Večja kot je več napada imamo.
* Z gumbi, ki so pri straneh polja za opremo (+1,-1,+5,-5) spreminjamo vrednost (DPS opreme), ki je prikazana na spodnjem delu kot graf. Z modro je označena trenutna vrednost.
* S pritiskom na moder kvadrat(pole za opremo) odpremo "Inventory", kjer imamo vse najdene predmete. Poleg imena imamo še povprečno vrednost, min in max vrednosti te opreme.

* Na desni(bojnem polju) vidimo našega bojevnika(moder kvadrat) in nasprotnika(rjav kvadrat) in njihove vrednosti za napad in število točk življenja(Hit points).
* Nastavimo težavnost in stopnjo(to spremeni hit point in DPS), lahko menjamo nasprotnika z gumbom "Next Enemy".
* Z gumbom "FIGHT" začnemo napad.

# To Do #
* Scaling ne deluje pravilno.
* Menjava opreme ne deluje pravilno(sploh ne deluje).
* Po vsakem premaganem nasprotniku se oprema(v Inventoryju) ponovno randomizira.